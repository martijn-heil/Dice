<center>![Dice](http://www.easymfne.net/images/dice.png)</center>
<br>
<center>[Source](https://gitlab.com/martijn-heil/Dice) |
[Change Log](https://gitlab.com/martijn-heil/Dice/commits/master) |
[Feature Request](https://gitlab.com/martijn-heil/Dice/issues) |
[Bug Report](https://gitlab.com/martijn-heil/Dice/issues)

<br>
<center>
Latest Release: v1.2 for Bukkit 1.7+
</center>

## About ##
This plugin was forked from [Martin Ambrus](https://github.com/martinambrus/Dice), who in turn forked his version from [EasyMFnE](https://github.com/EasyMFnE/Dice).
EasyMFnE wrote most of the code, Martin Ambrus partly removed the Plugin Metrics and added a `DiceRolled` event which other plugins can listen for.
After I (Ninjoh) forked it from Martin Ambrus, 
I removed the remaining references to Plugin Metrics and the plugin is now fully cleared of everything relating to Plugin Metrics.


I intend to support the plugin for at least as long as I will use it myself, and will probably passively support it afterwards, 
that means that I will likely always fix bugs when any are reported. Yet I make no guarantees, as I do this in my free time.


Dice is a very small Bukkit plugin designed to add an ability for players to roll dice, and is useful for role-playing.  Players can roll one die or many dice, with any number of sides.  The amount of players receiving dice-roll messages can be configured to match nearly any need.

## Features ##

* Players can roll dice (default and custom)
* Default numbers of dice and sides can be set in the configuration
* Result message can be global or limited by world/range

## Installation ##

1. Download Dice jar file.
2. Move/copy to your server's `plugins` folder.
3. Restart your server.
4. [**Optional**] Grant specific user permissions (see below).

## Permissions ##

* `dice.all` - Grant all permission nodes. (Default: `op`)
* `dice.reload` - Allow user to reload the plugin's configuration. (Default: `false`)
* `dice.roll` - Allow user to use the `/roll` command and default dice. (Default: `false`)
* `dice.roll.broadcast` - Broadcast the results of a player's rolls. (Default: `false`)
* `dice.roll.multiple` - Allow user to roll a custom number of dice. (Default: `false`)
* `dice.roll.multiple` - Allow user to roll custom-sided dice. (Default: `false`)

## Commands ##

Dice has only one command, `/roll`

* `/roll` - Roll the default dice
* `/roll <help,?>` - Show usage information
* `/roll reload` - Reload configuration from disk
* `/roll <count>` - Roll a custom number of default dice
* `/roll d<sides>` - Roll a default amount of custom-sided dice
* `/roll <count> d<sides>` - Roll custom number of custom-sided dice (parameter order does not matter)

## Configuration ##

At startup, the plugin will create a default configuration file if none exists.  This file is saved as `config.yml` and is located in `<server_folder>/plugins/Dice`.  Configuration nodes and expected values:

    default:
      sides: (integer >= 2)
      count: (integer >= 1)
    maximum:
      sides: (integer >= default.sides)
      count: (integer >= default.count)
    logging: (boolean, log rolls to console)
    broadcast:
      crossworld: (boolean, broadcast to all worlds)
      range: (integer > 0, broadcast range in blocks. -1 disables)
    messages:
      private: (String, sent to player if not broadcasting)
      broadcast: (String, sent to all players in range)

The `messages` configuration nodes will replace the following tags:

* `{PLAYER}` (The name of the player who rolled)
* `{RESULT}` (The result of the roll, as a list)
* `{COUNT}` (The number of dice rolled)
* `{SIDES}` (The number of sides per die)
* `{TOTAL}` (The total value of the roll)

## Bugs/Requests ##

This plugin is continually tested to ensure that it is performing correctly, but sometimes bugs can sneak in.  If you have found a bug with the plugin, or if you have a feature request, please [create an issue on Gitlab](https://gitlab.com/martijn-heil/Dice/issues).

## Privacy ##

This version of the Dice plugin has been fully wiped of any Plugin Metrics or statistics collection.

## License ##

This plugin is released as a free and open-source project under the [GNU General Public License version 3 (GPLv3)](http://www.gnu.org/copyleft/gpl.html).  To learn more about what this means, click that link or [read about it on Wikipedia](http://en.wikipedia.org/wiki/GNU_General_Public_License).
